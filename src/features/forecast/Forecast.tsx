import React from 'react';
import { Puff } from 'react-loading-icons';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { Day } from '../../components/day/Day';
import styles from './Forecast.module.css';
import { selectForecast } from './forecastSlice';

export function Forecast() {
  const forecast = useAppSelector(selectForecast);
  const dispatch = useAppDispatch();
  // const [incrementAmount, setIncrementAmount] = useState('2');
  // const incrementValue = Number(incrementAmount) || 0;

  return (
    <div>
      <h1>
        {forecast.status === 'loading' ? (
          <Puff stroke='black' className={styles.puff} />
        ) : (
          forecast.city.charAt(0).toUpperCase() + forecast.city.slice(1)
        )}
      </h1>
      {forecast.allDays.map((day, index) => (
        <Day key={index} day={day} />
      ))}
    </div>
  );
}
