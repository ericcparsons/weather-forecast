import axios from 'axios';
import moment from 'moment';
import { DayState, WeatherConditions } from './forecastSlice';

//TODO: move into database with endpoint, shouldn't live her (but fot the sake of DEMO this is fine)
const key = '2525128abc0e087c859f1345d47f4a84';

/**
 * used when a match cannot be found, a default forecast
 * @returns
 */
export const createEmptyForecast = () => {
  let emptyForecast: DayState[] = [];
  let emptyDay = {
    hi: 0,
    low: 0,
    weekday: '',
    conditions: WeatherConditions.Clear,
  };
  for (let i = 0; i < 5; i++) {
    emptyForecast.push(emptyDay);
  }
  return emptyForecast;
};

/**
 * fetch a 5 day forecast from Open Weather given lat and lon coords,
 * @param lat - latitude coords
 * @param lon - longitude coords
 * @param countryState - used to verify if a match was found
 * @returns a matching 5 day forecast or an empty forecast if a match was failed to be found
 */
export async function fetchForecast(lat: string, lon: string) {
  let flattenedForecast: DayState[] = [];

  const response = await axios
    .get(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&units=imperial&exclude=hourly,minutely,alerts&appid=${key}`
    )
    .then((res) => {
      flattenedForecast = res.data.daily.map((day: { [key: string]: any }) => {
        return {
          hi: Math.round(day.temp?.max),
          low: Math.round(day.temp?.min),
          weekday: moment.unix(day.dt).format('dddd'),
          conditions: day.weather[0].main,
        };
      });
    })
    .catch(() => {
      console.log(
        'Failed to find a forecast match from the lat/lon coords, returning an empty forecast'
      );
      flattenedForecast = createEmptyForecast();
    });

  //get only a 5 day forecast
  return flattenedForecast.slice(0, 5);
}

const createCoordObj = (match: { [key: string]: any }) => {
  return {
    lat: match.lat,
    lon: match.lon,
    countryState: match.state ? match.state : match.country,
  };
};

/**
 * get the lat and lon coordinates from Open Weather, based on city name
 * if a country or state was also specified, try to match specifically
 * @param city
 * @param countryOrState
 * @returns object containing the matching coords or an empty obj containing 0,0 coords
 */
export async function getCityLatLong(city: string, countryOrState: string) {
  const response = await axios
    .get(
      `https://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=5&appid=${key}`
    )
    .then((res: { [key: string]: any }) => {
      //if the user specified a comma followed by a country or state, find the result that matches it specifically
      if (countryOrState.length) {
        console.log(
          `country or state defined, matching on the closest result...`
        );
        const matchedCityCountry = res.data.filter(
          (possibleMatch: { [key: string]: any }) => {
            return possibleMatch.state.trim() === countryOrState.trim();
          }
        );
        //if we found a match based on the users input, return the lat/lon info for that match
        if (matchedCityCountry.length) {
          console.log('found a matching country or state!');
          return createCoordObj(matchedCityCountry[0]);
        } else {
          //otherwise give the user the first match
          return createCoordObj(res.data[0]);
        }
      } else {
        console.log(
          'no specified country or state, returning the first matching city value'
        );
        return createCoordObj(res.data[0]);
      }
    })
    .catch(async () => {
      console.log('failed to find a matching city, returning empty coords');
    });
  return response || { lat: 0, lon: 0, countryState: 'N/A' };
}
