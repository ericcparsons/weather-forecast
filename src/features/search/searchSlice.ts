import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

const initialState = {
  value: 'New York, New York',
  city: 'New York',
  countryOrState: 'New York',
};

export interface SearchState {
  value: string;
  city: string;
  countryOrState: string;
}

export const searchSlice = createSlice({
  name: 'searchTerm',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    updateSearchTerm: (state, action) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      return { value: action.payload, city: '', countryOrState: '' };
    },
    clearSearchTerm: (state) => {
      return (state = { value: '', city: '', countryOrState: '' });
    },
  },
});

export const { updateSearchTerm, clearSearchTerm } = searchSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectSearchTerm = (state: RootState) => state.search;

export default searchSlice.reducer;
