import React from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import {
  get5DayForecastAsync,
  selectForecast,
} from '../forecast/forecastSlice';
import styles from './Search.module.css';
import {
  clearSearchTerm,
  selectSearchTerm,
  updateSearchTerm,
} from './searchSlice';

export function Search() {
  const { value } = useAppSelector(selectSearchTerm);
  const dispatch = useAppDispatch();
  const searchIconUrl =
    'https://img.icons8.com/ios-glyphs/30/000000/search--v1.png';
  const clearIconUrl =
    'https://img.icons8.com/ios/25/000000/delete-sign--v1.png';

  const searchTermChangeHandler = (e: React.FormEvent<HTMLInputElement>) => {
    const userInput = e.currentTarget.value;
    dispatch(updateSearchTerm(userInput));
  };

  const searchTermClearChangeHandler = () => {
    dispatch(clearSearchTerm());
  };

  const handleKeywordKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key == 'Enter') {
      dispatch(get5DayForecastAsync({ value, city: '', countryOrState: '' }));
    }
  };

  // const dispatch = useAppDispatch();
  // const [incrementAmount, setIncrementAmount] = useState('2');
  // const incrementValue = Number(incrementAmount) || 0;

  return (
    <div id={styles.searchContainer}>
      <div>
        <img id={styles.searchIcon} alt='' src={searchIconUrl} />
        <input
          id={styles.search}
          type='text'
          value={value}
          onChange={searchTermChangeHandler}
          onKeyDown={handleKeywordKeyPress}
          placeholder='Search For a Location'
        />

        {value.length > 0 && (
          <button
            onClick={searchTermClearChangeHandler}
            type='button'
            id={styles.searchClearButton}
          >
            <img src={clearIconUrl} alt='' />
          </button>
        )}
      </div>
    </div>
  );
}
