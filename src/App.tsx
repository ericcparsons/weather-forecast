import React from 'react';
import './App.css';
import { useAppDispatch, useAppSelector } from './app/hooks';
import { Forecast } from './features/forecast/Forecast';
import { get5DayForecastAsync } from './features/forecast/forecastSlice';
import { Search } from './features/search/Search';
import { selectSearchTerm } from './features/search/searchSlice';
function App() {
  const dispatch = useAppDispatch();
  const searchTerm = useAppSelector(selectSearchTerm);

  //set initial forecast
  React.useEffect(() => {
    dispatch(get5DayForecastAsync(searchTerm));
  }, []);

  return (
    <div className='App'>
      <header>
        <Search />
      </header>
      <div className='Forecast'>
        <Forecast />
      </div>
    </div>
  );
}

export default App;
