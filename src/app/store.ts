import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import forecastSlice from '../features/forecast/forecastSlice';
import searchSlice from '../features/search/searchSlice';
export const store = configureStore({
  reducer: {
    forecast: forecastSlice,
    search: searchSlice,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
