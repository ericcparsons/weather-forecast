export const icons = {
  Clear:
    'https://img.icons8.com/external-tulpahn-detailed-outline-tulpahn/64/000000/external-sunny-sun-and-moon-tulpahn-detailed-outline-tulpahn-1.png',
  partlyCloudy:
    'https://img.icons8.com/external-dreamstale-lineal-dreamstale/64/000000/external-cloud-weather-dreamstale-lineal-dreamstale-4.png',
  Clouds:
    'https://img.icons8.com/external-dreamstale-lineal-dreamstale/64/000000/external-cloud-weather-dreamstale-lineal-dreamstale.png',
  Rain: 'https://img.icons8.com/external-dreamstale-lineal-dreamstale/64/000000/external-rain-weather-dreamstale-lineal-dreamstale-4.png',
  Storms:
    'https://img.icons8.com/external-dreamstale-lineal-dreamstale/64/000000/external-rain-weather-dreamstale-lineal-dreamstale-13.png',
  Snow: 'https://img.icons8.com/ios/64/000000/light-snow--v2.png',
};
