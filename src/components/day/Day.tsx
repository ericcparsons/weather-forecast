import React from 'react';
import { icons } from '../../app/icons';
import { DayState } from '../../features/forecast/forecastSlice';
import styles from './Day.module.css';

interface DayProps {
  day: DayState;
}

export const Day = ({ day }: DayProps) => {
  return (
    <div className={styles.day}>
      <h4>{day.weekday}</h4>
      <img src={icons[day.conditions]} />
      <div className={styles.temps}>
        <p>Hi: {day.hi}</p>
        <p>Low: {day.low}</p>
      </div>

      <p>{day.conditions}</p>
    </div>
  );
};
